package cn.accjiyun.ca.security;

import cn.accjiyun.ca.common.utils.DateUtils;
import cn.accjiyun.ca.entity.CAConfig;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.x509.X509V3CertificateGenerator;
import org.springframework.beans.factory.annotation.Autowired;

import javax.security.auth.x500.X500Principal;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Date;


public class BaseCert {
    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    protected static KeyPairGenerator kpg = null;

    public BaseCert() {
        try {
            // 采用 RSA 非对称算法加密
            kpg = KeyPairGenerator.getInstance("RSA");
            // 初始化为 1023 位
            kpg.initialize(1024);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }

    /**
     * 生成cer和pfx证书
     *
     * @param certPath
     */
    @Autowired
    public static void generateCert(String certPath, CAConfig caConfig) {
        FileOutputStream fileOutputStream = null;
        try {
            KeyPair keyPair = BaseCert.kpg.generateKeyPair();
            // 公钥
            PublicKey pubKey = keyPair.getPublic();
            // 私钥
            PrivateKey priKey = keyPair.getPrivate();
            X509V3CertificateGenerator certGen = new X509V3CertificateGenerator();
            // 设置序列号
            certGen.setSerialNumber(BigInteger.valueOf(System.currentTimeMillis()));
            // 设置颁发者
            certGen.setIssuerDN(new X500Principal(CAConfig.CA_ROOT_ISSUER));
            // 设置有效期
            certGen.setNotBefore(new Date());
            certGen.setNotAfter(DateUtils.addMonth(new Date(), 12));
            // 设置使用者
            certGen.setSubjectDN(new X500Principal(caConfig.getCA_DEFAULT_SUBJECT()));
            // 公钥
            certGen.setPublicKey(pubKey);
            // 签名算法
            certGen.setSignatureAlgorithm(caConfig.CA_ALGORITHM);
            X509Certificate cert = certGen.generateX509Certificate(priKey, "BC");
            // 导出为 cer 证书
            fileOutputStream = new FileOutputStream(certPath + ".cer");
            fileOutputStream.write(cert.getEncoded());
            fileOutputStream.close();
            // 创建KeyStore
            KeyStore store = KeyStore.getInstance(CAConfig.PKCS12);
            store.load(null, null);
            store.setKeyEntry(caConfig.getPK_ALIAS(), priKey, caConfig.getPK_PASSWORD().toCharArray(), new Certificate[]{cert});
            //导出为pfx证书
            fileOutputStream = new FileOutputStream(certPath + ".pfx");
            store.store(fileOutputStream, caConfig.getPK_PASSWORD().toCharArray());
            fileOutputStream.write(cert.getEncoded());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}