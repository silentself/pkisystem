package cn.accjiyun.ca.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by jiyun on 2017/5/23.
 */
@Entity
@Table(name = "user_puk", schema = "ca_server_db", catalog = "")
public class UserPuk implements Serializable {
    private int userId;
    private String pukUrl;
    private String pfxUrl;

    @Id
    @Column(name = "user_id", nullable = false)
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "puk_url", nullable = true, length = 255)
    public String getPukUrl() {
        return pukUrl;
    }

    public void setPukUrl(String pukUrl) {
        this.pukUrl = pukUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserPuk userPuk = (UserPuk) o;

        if (userId != userPuk.userId) return false;
        if (pukUrl != null ? !pukUrl.equals(userPuk.pukUrl) : userPuk.pukUrl != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + (pukUrl != null ? pukUrl.hashCode() : 0);
        return result;
    }

    @Basic
    @Column(name = "pfx_url", nullable = true, length = 255)
    public String getPfxUrl() {
        return pfxUrl;
    }

    public void setPfxUrl(String pfxUrl) {
        this.pfxUrl = pfxUrl;
    }
}
