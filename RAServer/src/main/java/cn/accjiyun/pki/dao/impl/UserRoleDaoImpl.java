package cn.accjiyun.pki.dao.impl;

import cn.accjiyun.pki.common.dao.DaoSupport;
import cn.accjiyun.pki.common.entity.PageModel;
import cn.accjiyun.pki.common.entity.QueryEntity;
import cn.accjiyun.pki.dao.UserRoleDao;
import cn.accjiyun.pki.entity.UserRole;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jiyun on 2017/5/17.
 */
@Repository("userRoleDao")
public class UserRoleDaoImpl extends DaoSupport<UserRole> implements UserRoleDao {
    /**
     * 创建用户角色
     *
     * @param userRole 用户角色实体
     * @return 用户角色ID
     */
    @Override
    public int createUserRole(UserRole userRole) {
        save(userRole);
        return userRole.getRoleId();
    }

    /**
     * 通过用户角色ID数组删除用户角色
     *
     * @param userRoleIds 用户角色ID数组
     */
    @Override
    public void deleteUserRole(int[] userRoleIds) {
        for (int i = 0; i < userRoleIds.length; i++) {
            delete(userRoleIds[i]);
        }
    }

    /**
     * 通过用户角色ID查询用户角色信息
     *
     * @param userRoleId 庭成员ID
     * @return 用户角色实体
     */
    @Override
    public UserRole queryUserRoleById(int userRoleId) {
        return get(userRoleId);
    }

    /**
     * 通过角色名称查询对应ID
     *
     * @param roleName 角色名称
     * @return 角色ID
     */
    @Override
    public int queryUserRoleIdByName(String roleName) {
        Query query = getSession().createQuery("select r.roleId from UserRole as r where r.roleName=?");
        Object[] queryParams = {roleName};
        setQueryParams(query, queryParams);
        return (Integer) (query.list().get(0));
    }

    /**
     * 分页查询用户角色信息
     *
     * @param queryEntity 查询条件实体
     * @param model       分页实体
     * @return 用户角色实体列表
     */
    @Override
    public List<UserRole> queryUserRolePage(QueryEntity queryEntity, PageModel<UserRole> model) {
        return queryByCriteria(queryEntity, model);
    }

    /**
     * 获取用户角色总数
     *
     * @return 用户角色总数
     */
    @Override
    public long queryAllUserRoleCount() {
        return getCount();
    }

    /**
     * 利用hql语句查找用户角色信息列表
     *
     * @param hql         查询语句
     * @param queryParams 查询参数
     * @return 用户角色实体列表
     */
    @Override
    public List<UserRole> createQuery(String hql, Object[] queryParams) {
        Query query = getSession().createQuery(hql);
        setQueryParams(query, queryParams);
        return query.list();
    }

    /**
     * 更新用户角色信息
     *
     * @param userRole 用户角色实体
     */
    @Override
    public void updateUserRole(UserRole userRole) {
        saveOrUpdate(userRole);
    }
}
