package cn.accjiyun.pki.dao.impl;

import cn.accjiyun.pki.common.dao.DaoSupport;
import cn.accjiyun.pki.common.entity.PageModel;
import cn.accjiyun.pki.common.entity.QueryEntity;
import cn.accjiyun.pki.dao.TaxDao;
import cn.accjiyun.pki.entity.Tax;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jiyun on 2017/5/17.
 */
@Repository("taxDao")
public class TaxDaoImpl extends DaoSupport<Tax> implements TaxDao {
    /**
     * 创建税务账单
     *
     * @param tax 税务账单实体
     * @return 税务账单ID
     */
    @Override
    public int createTax(Tax tax) {
        save(tax);
        return tax.getId();
    }

    /**
     * 通过税务账单ID数组删除税务账单
     *
     * @param taxIds 税务账单ID数组
     */
    @Override
    public void deleteTax(int[] taxIds) {
        for (int i = 0; i < taxIds.length; i++) {
            delete(taxIds[i]);
        }
    }

    /**
     * 通过税务账单ID查询税务账单信息
     *
     * @param taxId 庭成员ID
     * @return 税务账单实体
     */
    @Override
    public Tax queryTaxById(int taxId) {
        return get(taxId);
    }

    /**
     * 分页查询税务账单信息
     *
     * @param queryEntity 查询条件实体
     * @param model       分页实体
     * @return 税务账单实体列表
     */
    @Override
    public List<Tax> queryTaxPage(QueryEntity queryEntity, PageModel<Tax> model) {
        return queryByCriteria(queryEntity, model);
    }

    /**
     * 获取税务账单总数
     *
     * @return 税务账单总数
     */
    @Override
    public long queryAllTaxCount() {
        return getCount();
    }

    /**
     * 利用hql语句查找税务账单信息列表
     *
     * @param hql         查询语句
     * @param queryParams 查询参数
     * @return 税务账单实体列表
     */
    @Override
    public List<Tax> createQuery(String hql, Object[] queryParams) {
        Query query = getSession().createQuery(hql);
        setQueryParams(query, queryParams);
        return query.list();
    }

    /**
     * 更新税务账单信息
     *
     * @param tax 税务账单实体
     */
    @Override
    public void updateTax(Tax tax) {
        saveOrUpdate(tax);
    }
}
