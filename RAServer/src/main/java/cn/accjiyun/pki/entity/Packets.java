package cn.accjiyun.pki.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by jiyun on 2017/6/1.
 */
@Entity
public class Packets implements Serializable {
    private int id;
    private String cipherAesKey;
    private String encryptContent;
    private String textSign;
    private String fileSign;
    private Tax taxById;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "cipherAESKey", nullable = true, length = -1)
    public String getCipherAesKey() {
        return cipherAesKey;
    }

    public void setCipherAesKey(String cipherAesKey) {
        this.cipherAesKey = cipherAesKey;
    }

    @Basic
    @Column(name = "encrypt_content", nullable = true, length = -1)
    public String getEncryptContent() {
        return encryptContent;
    }

    public void setEncryptContent(String encryptContent) {
        this.encryptContent = encryptContent;
    }

    @Basic
    @Column(name = "text_sign", nullable = true, length = -1)
    public String getTextSign() {
        return textSign;
    }

    public void setTextSign(String textSign) {
        this.textSign = textSign;
    }

    @Basic
    @Column(name = "file_sign", nullable = true, length = -1)
    public String getFileSign() {
        return fileSign;
    }

    public void setFileSign(String fileSign) {
        this.fileSign = fileSign;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Packets packets = (Packets) o;

        if (id != packets.id) return false;
        if (cipherAesKey != null ? !cipherAesKey.equals(packets.cipherAesKey) : packets.cipherAesKey != null)
            return false;
        if (encryptContent != null ? !encryptContent.equals(packets.encryptContent) : packets.encryptContent != null)
            return false;
        if (textSign != null ? !textSign.equals(packets.textSign) : packets.textSign != null) return false;
        if (fileSign != null ? !fileSign.equals(packets.fileSign) : packets.fileSign != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (cipherAesKey != null ? cipherAesKey.hashCode() : 0);
        result = 31 * result + (encryptContent != null ? encryptContent.hashCode() : 0);
        result = 31 * result + (textSign != null ? textSign.hashCode() : 0);
        result = 31 * result + (fileSign != null ? fileSign.hashCode() : 0);
        return result;
    }

    @OneToOne
    @JoinColumn(name = "id", referencedColumnName = "id", nullable = false)
    public Tax getTaxById() {
        return taxById;
    }

    public void setTaxById(Tax taxById) {
        this.taxById = taxById;
    }
}
