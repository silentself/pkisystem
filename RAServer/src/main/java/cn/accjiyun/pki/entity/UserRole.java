package cn.accjiyun.pki.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by jiyun on 2017/6/1.
 */
@Entity
@Table(name = "user_role", schema = "pki_system_db", catalog = "")
public class UserRole implements Serializable {
    private int roleId;
    private String roleName;
    private String menuJson;

    @Id
    @Column(name = "role_id", nullable = false)
    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    @Basic
    @Column(name = "role_name", nullable = true, length = 30)
    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @Basic
    @Column(name = "menu_json", nullable = true, length = -1)
    public String getMenuJson() {
        return menuJson;
    }

    public void setMenuJson(String menuJson) {
        this.menuJson = menuJson;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserRole userRole = (UserRole) o;

        if (roleId != userRole.roleId) return false;
        if (roleName != null ? !roleName.equals(userRole.roleName) : userRole.roleName != null) return false;
        if (menuJson != null ? !menuJson.equals(userRole.menuJson) : userRole.menuJson != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = roleId;
        result = 31 * result + (roleName != null ? roleName.hashCode() : 0);
        result = 31 * result + (menuJson != null ? menuJson.hashCode() : 0);
        return result;
    }
}
