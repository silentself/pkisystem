package cn.accjiyun.pki.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by jiyun on 2017/6/1.
 */
@Entity
@Table(name = "system_user", schema = "pki_system_db", catalog = "")
public class SystemUser implements Serializable {
    private int id;
    private String loginName;
    private String password;
    private byte disabled;
    private String username;
    private String identityCard;
    private String mobile;
    private String pukUrl;
    private UserRole userRoleByRoleId;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "login_name", nullable = false, length = 20)
    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    @Basic
    @Column(name = "password", nullable = true, length = 64)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "disabled", nullable = false)
    public byte getDisabled() {
        return disabled;
    }

    public void setDisabled(byte disabled) {
        this.disabled = disabled;
    }

    @Basic
    @Column(name = "username", nullable = true, length = 15)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "identity_card", nullable = true, length = 20)
    public String getIdentityCard() {
        return identityCard;
    }

    public void setIdentityCard(String identityCard) {
        this.identityCard = identityCard;
    }

    @Basic
    @Column(name = "mobile", nullable = true, length = 15)
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Basic
    @Column(name = "puk_url", nullable = true, length = 255)
    public String getPukUrl() {
        return pukUrl;
    }

    public void setPukUrl(String pukUrl) {
        this.pukUrl = pukUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SystemUser that = (SystemUser) o;

        if (id != that.id) return false;
        if (disabled != that.disabled) return false;
        if (loginName != null ? !loginName.equals(that.loginName) : that.loginName != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        if (identityCard != null ? !identityCard.equals(that.identityCard) : that.identityCard != null) return false;
        if (mobile != null ? !mobile.equals(that.mobile) : that.mobile != null) return false;
        if (pukUrl != null ? !pukUrl.equals(that.pukUrl) : that.pukUrl != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (loginName != null ? loginName.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (int) disabled;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (identityCard != null ? identityCard.hashCode() : 0);
        result = 31 * result + (mobile != null ? mobile.hashCode() : 0);
        result = 31 * result + (pukUrl != null ? pukUrl.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "role_id", referencedColumnName = "role_id")
    public UserRole getUserRoleByRoleId() {
        return userRoleByRoleId;
    }

    public void setUserRoleByRoleId(UserRole userRoleByRoleId) {
        this.userRoleByRoleId = userRoleByRoleId;
    }
}
