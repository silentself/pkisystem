package cn.accjiyun.pki.controller.webfront;

import cn.accjiyun.pki.common.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by jiyun on 2017/2/1.
 */
@Controller
public class WebFrontController extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(WebFrontController.class);

    /**
     * 首页获取网站首页数据
     */
    @RequestMapping("/index")
    public String getIndexpage() {
        return "redirect:/admin";
    }

}
