package cn.accjiyun.pki.controller.webbackend;

import cn.accjiyun.pki.common.controller.BaseController;
import cn.accjiyun.pki.common.utils.SingletonLoginUtils;
import cn.accjiyun.pki.entity.SystemUser;
import cn.accjiyun.pki.service.SystemUserService;
import cn.accjiyun.pki.service.TaxService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by jiyun on 2017/2/5.
 */
@Controller
@RequestMapping("/admin/main")
public class MainController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(MainController.class);
    private static String mainPage = getViewPath("/admin//main/main");//后台管理主界面
    private static String mainIndexPage = getViewPath("/admin/main/welcome");//后台操作中心初始化首页
    private static String loginFail = "redirect:/admin";//后台管理主界面

    @Autowired
    private SystemUserService systemUserService;
    @Autowired
    private TaxService taxService;


    /**
     * 进入操作中心
     *
     * @param request
     * @return ModelAndView
     */
    @RequestMapping("/main")
    public ModelAndView mainPage(HttpServletRequest request) {
        ModelAndView model = new ModelAndView();
        try {
            SystemUser systemUser = SingletonLoginUtils.getLoginSystemUser(request);
            if (systemUser != null) {
                model.setViewName(mainPage);
                model.addObject("systemUser", systemUser);
            } else {
                model.setViewName(loginFail);
            }
        } catch (Exception e) {
            model.setViewName(this.setExceptionRequest(request, e));
            logger.error("MainController.mainPage()--error", e);
        }
        return model;
    }

    /**
     * 后台操作中心初始化首页
     *
     * @param request
     * @return ModelAndView
     */
    @RequestMapping("/welcome")
    public ModelAndView mainIndex(HttpServletRequest request) {
        ModelAndView model = new ModelAndView();
        try {
            SystemUser systemUser = SingletonLoginUtils.getLoginSystemUser(request);
            model.addObject("systemUserCount", systemUserService.queryAllSystemUserCount());
            model.addObject("taxCount", taxService.queryAllTaxCount());
            model.addObject("systemUser", systemUser);
            model.addObject("roleId", systemUser.getUserRoleByRoleId().getRoleId());
            model.addObject("role", systemUser.getUserRoleByRoleId());
            model.setViewName(mainIndexPage);
        } catch (Exception e) {
            model.setViewName(this.setExceptionRequest(request, e));
            logger.error("MainController.mainIndex()---error", e);
        }
        return model;
    }

    /**
     * 访问权限受限制跳转
     *
     * @return ModelAndView
     */
    @RequestMapping("/loginFail")
    public ModelAndView notFunctionMsg() {
        ModelAndView model = new ModelAndView();
        model.addObject("message", "对不起，您没有操作权限！");
        model.setViewName(loginFail);
        return model;
    }

}
