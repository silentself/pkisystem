package cn.accjiyun.pki.service.impl;

import cn.accjiyun.pki.common.entity.PageModel;
import cn.accjiyun.pki.common.entity.QueryEntity;
import cn.accjiyun.pki.dao.SystemUserDao;
import cn.accjiyun.pki.dao.UserRoleDao;
import cn.accjiyun.pki.entity.SystemUser;
import cn.accjiyun.pki.entity.UserRole;
import cn.accjiyun.pki.service.SystemUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jiyun on 2017/5/17.
 */
@Service("systemUserService")
public class SystemUserServiceImpl implements SystemUserService {

    @Autowired
    private SystemUserDao systemUserDao;
    @Autowired
    private UserRoleDao userRoleDao;
    /**
     * 创建系统用户
     *
     * @param user 系统用户实体
     * @return 系统用户ID
     */
    @Override
    public int createSystemUser(SystemUser user) {
        return systemUserDao.createSystemUser(user);
    }

    /**
     * 通过系统用户ID数组删除系统用户
     *
     * @param userIds 系统用户ID数组
     */
    @Override
    public void deleteSystemUser(int[] userIds) {
        systemUserDao.deleteSystemUser(userIds);
    }

    /**
     * 通过系统用户ID查询系统用户信息
     *
     * @param userId 庭成员ID
     * @return 系统用户实体
     */
    @Override
    public SystemUser querySystemUserById(int userId) {
        return systemUserDao.querySystemUserById(userId);
    }

    /**
     * 分页查询系统用户信息
     *
     * @param queryEntity 查询条件实体
     * @param model       分页实体
     * @return 系统用户实体列表
     */
    @Override
    public List<SystemUser> querySystemUserPage(QueryEntity queryEntity, PageModel<SystemUser> model) {
        return systemUserDao.querySystemUserPage(queryEntity, model);
    }

    /**
     * 获取系统用户总数
     *
     * @return 系统用户总数
     */
    @Override
    public long queryAllSystemUserCount() {
        return systemUserDao.queryAllSystemUserCount();
    }

    /**
     * 登录查询系统用户实体
     *
     * @param user 系统用户实例
     * @return 如果存在用户，返回完整实体，否则为空
     */
    @Override
    public SystemUser queryLoginUser(SystemUser user) {
        return systemUserDao.queryLoginUser(user);
    }

    /**
     * 通过用户角色名称查询用户列表
     *
     * @param roleName 角色名称
     * @return 对应角色的用户列表
     */
    @Override
    public List<SystemUser> querySystemUserByRoleName(String roleName) {
        int roleId = userRoleDao.queryUserRoleIdByName(roleName);
        PageModel<SystemUser> pageModel  = new PageModel<>();
        pageModel.setPageSize((int) systemUserDao.queryAllSystemUserCount());
        pageModel.setPageNo(1);
        QueryEntity queryEntity = new QueryEntity();
        Map eqParams = new HashMap();
        eqParams.put("userRoleByRoleId.roleId", roleId);
        queryEntity.setEqParams(eqParams);
        return systemUserDao.querySystemUserPage(queryEntity, pageModel);
    }

    /**
     * 通过用户角色ID查询用户列表
     *
     * @param roleId 角色ID
     * @return 对应角色的用户列表
     */
    @Override
    public List<SystemUser> querySystemUserByRoleId(int roleId) {
        PageModel<SystemUser> pageModel  = new PageModel<>();
        pageModel.setPageSize((int) systemUserDao.queryAllSystemUserCount());
        pageModel.setPageNo(1);
        QueryEntity queryEntity = new QueryEntity();
        Map eqParams = new HashMap();
        eqParams.put("userRoleByRoleId.roleId", roleId);
        queryEntity.setEqParams(eqParams);
        return systemUserDao.querySystemUserPage(queryEntity, pageModel);
    }

    /**
     * 通过用户姓名查询用户
     *
     * @param username 用户姓名
     * @return 对应姓名的用户列表
     */
    @Override
    public List<SystemUser> querySystemUserByUsername(String username) {
        Object[] queryParams = {username};
        String hql = "from SystemUser as user where user.username=?";
        return createQuery(hql, queryParams);
    }

    /**
     * 通过用户角色ID查询用户角色信息
     *
     * @param userRoleId 庭成员ID
     * @return 用户角色实体
     */
    @Override
    public UserRole queryUserRoleById(int userRoleId) {
        return userRoleDao.queryUserRoleById(userRoleId);
    }

    /**
     * 通过角色名称查询对应ID
     *
     * @param roleName 角色名称
     * @return 角色ID
     */
    @Override
    public int queryUserRoleIdByName(String roleName) {
        return userRoleDao.queryUserRoleIdByName(roleName);
    }

    /**
     * Validate the loginName if exist.
     *
     * @param loginName
     * @return true if the loginName exist.
     */
    @Override
    public boolean validateLoginName(String loginName) {
        return systemUserDao.validateLoginName(loginName);
    }

    /**
     * 利用hql语句查找系统用户信息列表
     *
     * @param hql         查询语句
     * @param queryParams 查询参数
     * @return 系统用户实体列表
     */
    @Override
    public List<SystemUser> createQuery(String hql, Object[] queryParams) {
        return systemUserDao.createQuery(hql, queryParams);
    }


    /**
     * 更新系统用户信息
     *
     * @param user 系统用户实体
     */
    @Override
    public void updateSystemUser(SystemUser user) {
        systemUserDao.updateSystemUser(user);
    }

    /**
     * 更新用户启用状态
     *
     * @param userId 用户ID
     * @param status 状态0:正常,1:冻结
     */
    @Override
    public void updateDisableOrStartUser(int userId, int status) {
        systemUserDao.updateDisableOrStartUser(userId, status);
    }
}
