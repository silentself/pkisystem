package cn.accjiyun.pki.service;

import cn.accjiyun.pki.common.entity.PageModel;
import cn.accjiyun.pki.common.entity.QueryEntity;
import cn.accjiyun.pki.entity.Packets;
import cn.accjiyun.pki.entity.Tax;

import java.util.List;

/**
 * Created by jiyun on 2017/5/18.
 */
public interface PacketsService {
    /**
     * 创建数据包
     * @param packets 数据包实体
     * @return 数据包ID
     */
    public int createPackets(Packets packets);

    /**
     * 通过数据包ID数组删除数据包
     * @param packetsIds 数据包ID数组
     */
    public void deletePackets(int[] packetsIds);

    /**
     * 通过数据包ID查询数据包信息
     * @param packetsId 庭成员ID
     * @return 数据包实体
     */
    public Packets queryPacketsById(int packetsId);

    /**
     * 分页查询数据包信息
     * @param queryEntity 查询条件实体
     * @param model 分页实体
     * @return 数据包实体列表
     */
    public List<Packets> queryPacketsPage(QueryEntity queryEntity, PageModel<Packets> model);

    /**
     * 更新数据包信息
     * @param packets 数据包实体
     */
    public void updatePackets(Packets packets);

    /**
     * 获取数据包总数
     * @return 数据包总数
     */
    public long queryAllPacketsCount();

    /**
     * 利用hql语句查找数据包信息列表
     * @param hql 查询语句
     * @param queryParams 查询参数
     * @return 数据包实体列表
     */
    public List<Packets> createQuery(final String hql, final Object[] queryParams);

    /**
     * 根据数字签名审核验证
     *
     * @param tax
     * @return
     */
    public boolean audit(Tax tax, String realAttachUrl);

    /**
     * 将申报信息加密、生成数字签名打包
     *
     * @param packets       数据包
     * @param content       申报信息
     * @param realAttachUrl 附件路径
     * @param pfxPath       申报人pfx证书路径
     * @param password      申报人pfx证书密码
     * @return 打包后的Packers数据包实例
     */
    public Packets packedMessage(Packets packets, String content, String realAttachUrl,
                                 String pfxPath, String password);
}
