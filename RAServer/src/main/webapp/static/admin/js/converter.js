/**
 * Created by jiyun on 2017/4/6.
 */

function sexToInt(sex) {
    return sex == "G" || sex == "1" || sex == "男" ? 1 : 0;
}

function getUserRoleName(roleId) {
    switch (roleId) {
        case 0:
            return "申报人";
        case 1:
            return "承办人";
        case 99:
            return "超级管理员";
        default:
            return "未分类";
    }
}

function getTaxTypeName(taxTypeId) {
    switch (taxTypeId) {
        case 1:
            return "个人所得税";
        case 2:
            return "企业税";
        case 3:
            return "印花税";
        default:
            return "未分类";
    }
}

function getStatusName(status) {
    switch (status) {
        case 0:
            return "未审核";
        default:
            return "已审核";
    }
}

function sexToInt(sex) {
    return sex == "G" || sex == "1" || sex == "男" ? 1 : 0;
}


