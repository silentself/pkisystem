<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/layouts/admin/admin-header.jsp" %>
<style>
    .cert {
        margin: 30px;
    }
</style>
<body>
<div class="container-fluid larry-wrapper">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <section class="panel panel-padding" style="text-align: center;">
                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 50px;">
                    <legend>证书申请</legend>
                </fieldset>
                <br/>
                <br/>
                <form id="form1" class="layui-form layui-form-pane" name="certForm"
                      action="/admin/cert/generateCert">
                    <div class="layui-form-item" style="text-align: center;">
                        <input type="password" name="password" id="password" required jq-verify="required"
                               jq-error="请输入证书密码" placeholder="证书密码" autocomplete="off" class="layui-input ">
                    </div>
                    <div class="layui-form-item">
                        <button class="layui-btn" jq-submit lay-filter="submit">申请证书</button>
                    </div>
                </form>
                <div class="layui-form-item">
                    <div class="cert">
                        <%--<button class="layui-btn ajax" data-params='{"url": "/admin/cert/getPukCert"}'>--%>
                            <%--<i class="iconfont">&#xe60a;</i>下载Cer证书--%>
                        <%--</button>--%>
                        <button class="layui-btn" onclick="window.open('/admin/cert/getPukCert')">
                            <i class="iconfont">&#xe60a;</i>下载Cer证书
                        </button>
                        <button class="layui-btn" onclick="downPfx()">
                            <i class="iconfont">&#xe60a;</i>下载Pfx证书
                        </button>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
</body>
<script src="/static/admin/js/layui/layui.js"></script>
<%@include file="/WEB-INF/layouts/admin/layui-version.jsp" %>
<script>
    layui.use('myform');

    function downPfx() {
        var password = document.getElementById('password').valueOf().value;
        if (password.length <= 0) {
            alert("请输入密码！");
            return;
        }
        window.open('/admin/cert/getPfxCert?password=' + password);
    }
</script>
</html>