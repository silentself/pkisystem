<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script id="list-tpl" type="text/html"
        data-params='{"url":"/admin/system/getUserList?pageNo=1","pageid":"#page","dataName":"systemUserData"}'>
    <table id="example" class="layui-table lay-even" data-name="systemUserData" data-tplid="list-tpl">
        <thead>
        <tr>
            <th><input type="checkbox" id="checkall" data-name="id" lay-filter="check" lay-skin="primary"></th>
            <th>登录账号</th>
            <th>角色</th>
            <th>姓名</th>
            <th>身份证</th>
            <th>手机</th>
            <th>状态</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        {{# layui.each(d.list, function(index, item){ }}
        <tr style="text-align: center">
            <td><input type="checkbox" name="id" value="{{ item.id}}" lay-skin="primary"></td>
            <td>{{ item.loginName }}</td>
            <td>{{ getUserRoleName(item.roleId) }}</td>
            <td>{{ item.username }}</td>
            <td>{{ item.identityCard }}</td>
            <td>{{ item.mobile }}</td>
            <td>
                <input type="checkbox" name="disabled" lay-skin="switch" lay-text="启用|禁用" {{#if (item.disabled== 0){
                       }}checked="checked" {{# } }} lay-filter="ajax"
                       data-params='{"url":"/admin/system/updateStatus","data":"id={{ item.id}}"}' />
            </td>
            <td>
                <button class="layui-btn layui-btn-mini modal-catch" data-params='{"content": ".add-subcat","area":"700px,500px",
                "title": "编辑 ：{{item.username}}","key":"id={{ item.id }}","type":"1"}'>
                    <i class="iconfont">&#xe653;</i>编辑
                </button>
                <button class="layui-btn layui-btn-mini layui-btn-danger ajax"
                        data-params='{"url": "/admin/system/delete?id={{item.id}}","loading":"true"}'>
                    <i class="iconfont">&#xe626;</i>删除
                </button>
            </td>
        </tr>
        {{# }); }}
        </tbody>
    </table>
    {{#  if(d.list.length === 0){ }}
    无数据
    {{#  } }}
</script>