<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script id="list-tpl" type="text/html" data-params='{"url":"/admin/tax/getTaxList?pageNo=1","pageid":"#page","dataName":"taxData"}'>
    <table id="example" class="layui-table lay-even" data-name="taxData" data-tplid="list-tpl">
        <thead>
        <tr>
            <th><input type="checkbox" id="checkall" data-name="id" lay-filter="check" lay-skin="primary"></th>
            <th>姓名</th>
            <th>收入</th>
            <th>税费</th>
            <th>税务类型</th>
            <th>创建时间</th>
            <th>审核</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        {{# layui.each(d.list, function(index, item){ }}
        <tr style="text-align: center">
            <td><input type="checkbox" name="id" value="{{ item.id}}" lay-skin="primary"></td>
            <td>{{ item.username}}</td>
            <td>{{ item.income}}</td>
            <td>{{ item.tax}}</td>
            <td>{{ getTaxTypeName(item.taxTypeId) }}</td>
            <td>{{ item.createTime }}</td>
            <td>
                <input type="checkbox" {{#if(item.roleId == 99){}} disabled="disabled" {{#} }} name="status" lay-skin="switch" lay-text="已审|待审" {{#if (item.status== 1){
                       }}checked="checked" {{# } }} lay-filter="ajax"
                       data-params='{"url":"/admin/tax/updateStatus","data":"id={{ item.id}}"}' />
            </td>
            <td>
                <button class="layui-btn layui-btn-mini modal-catch" data-params='{"content": ".add-subcat","area":"800px,600px",
                "title": "查看 ：{{item.username}}","key":"id={{ item.id }}","type":"1"}'>
                <i class="iconfont">&#xe653;</i>查看
            </button>
                <button class="layui-btn layui-btn-mini layui-btn-danger ajax"
                        data-params='{"url": "/admin/tax/delete?id={{item.id}}","loading":"true"}}'>
                    <i class="iconfont">&#xe626;</i>删除
                </button>
            </td>
        </tr>
        {{# }); }}
        </tbody>
    </table>
    {{#  if(d.list.length === 0){ }}
    无数据
    {{#  } }}
</script>